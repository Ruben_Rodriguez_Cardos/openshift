from flask import Flask, flash, redirect, render_template, request, session, abort, g
import pymysql.cursors

application = Flask(__name__)
application.secret_key = 'root'

#@application.route("/")
#def hello():
#	connection = pymysql.connect(host='10.131.8.184', user='userGM8', password='ANdu4i5R0KIQU7ph', db='sampledb')
#	with connection.cursor() as cursor:
#		sql = "SELECT * FROM People"
#		cursor.execute(sql)
#		result = cursor.fetchone()
#		connection.close()
#	return "Hello World "+str(result[1])

@application.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        #Veo quien a pasado por aqui (De prueba de momento solo uno)
        users =" "
        connection = pymysql.connect(host='10.131.8.184', user='userGM8', password='ANdu4i5R0KIQU7ph', db='sampledb')
        with connection.cursor() as cursor:
            sql = "SELECT * FROM People"
            cursor.execute(sql)
            result = cursor.fetchall()
            users = []
            for i in result:
                users.append(i[1])
        connection.close()
       	return render_template('main.html',users = users)
 
@application.route('/login', methods=['POST'])
def do_admin_login():
    connection = pymysql.connect(host='10.131.8.184', user='userGM8', password='ANdu4i5R0KIQU7ph', db='sampledb')
    print("DDBB connected!")
    #Añadir el login a la base de datos
    with connection.cursor() as cursor:
        sql = "SELECT MAX(id) FROM People;"
        cursor.execute(sql)
        result = cursor.fetchall()
        aux_id = int(result[len(result)-1][0]) + 1
        print(type(aux_id),type(request.form['username']))
        sql = "INSERT INTO People VALUES (%s,%s);"
        cursor.execute(sql,(aux_id,request.form['username']))
    connection.commit()
    connection.close()
    session['logged_in'] = True
    return home()

@application.route("/logout")
def logout():
    session['logged_in'] = False
    return home()

if __name__ == "__main__":
	application.run()